const { src, dest, task, watch, series } = require('gulp'),
	browserSync = require('browser-sync').create(),
	php = require('gulp-connect-php'),
	del = require('del'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),
	imagemin = require('gulp-imagemin'),
	minify = require('gulp-minify'),
	svgSprite = require('gulp-svg-sprite');

// SVG Config
let config = {
	mode: {
		symbol: {
			render: {
				css: false,
				scss: false
			},
			dest: '',
			prefix: '.svg-',
			sprite: 'sprite.svg',
			example: true
		}
	}
};

let backAssets = '../back/wp-content/themes/4r-site/assets/';

task('clean:dist', function () {
	return del('assets/js/dist/**', { force: true });
});

task('php', function () {
	//OSX

	php.server(
		{
			base: './',
			port: 8000,
			keepalive: true
		},
		function () {
			browserSync.init({
				proxy: 'localhost:8000',
				baseDir: './',
				open: true,
				notify: false
			});

			watch('assets/sass/**/*.scss', series('sass'));
			watch('./*.php').on('change', browserSync.reload);
			watch('assets/js/**/*.js').on('change', browserSync.reload);
			watch('assets/svg/**/*.svg', series('svg'));
		}
	);

	//WINDOWS

	// browserSync.init({
	//     proxy: 'localhost:80',
	//     baseDir: './tiny-4r-wp/front',
	//     open: true,
	//     notify: false
	// });

	// watch("assets/sass/**/*.scss", series('sass'));
	// watch("./*.php").on('change', browserSync.reload);
});

task('serve', series('php'));

// Compile sass into CSS & auto-inject into browsers
task('sass', function () {
	return src('assets/sass/**/*')
		.pipe(sass({ outputStyle: 'compressed' }))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
		.pipe(dest('assets/css'))
		.pipe(browserSync.stream());
});

// make minify images
task('build-img', function () {
	return src('../../uploads/**/').pipe(imagemin()).pipe(dest('../../uploads'));
});

// make minify js
task('compressjs', function () {
	return src(['assets/js/**/!(*.min).js'])
		.pipe(minify())
		.pipe(dest('assets/js/dist/'));
});

task('svg', function () {
	return src('**/*.svg', { cwd: 'assets/svg' })
		.pipe(svgSprite(config))
		.pipe(dest('assets/images'));
});

task('deployjs', series(['clean:dist', 'compressjs']));

task('default', series('serve'));

task('deploy', series(['deployjs', 'sass']));

task('copy-back', function () {
	return src(['assets/**/*']).pipe(dest(backAssets));
});
