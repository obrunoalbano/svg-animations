<?php 

include('partials/header.php');

?>


<div class="page-content -single">
    <div class="page-wrap -with-sidebar">
        <div class="container">
			<div class="page-inner">
                <div class="cp-card">
                    <div class="cp-card__header">
                        <figure class="cp-card__figure">
                            <img src="assets/images/img-post.png"/>
                        </figure>
                        <div class="cp-card__content">
                            <h3 class="cp-card__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h3>
                        </div>
                        <div class="cp-card__bottom">
                            <ul class="cp-card__meta">
                                <li>3:45pm</li>
                                <li>17 - oct -2021</li>       
                            </ul>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde sequi doloribus, impedit nemo numquam nobis voluptatem, expedita, omnis ea eius corrupti? Tempore exercitationem adipisci blanditiis quia id quas, odio magnam.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde sequi doloribus, impedit nemo numquam nobis voluptatem, expedita, omnis ea eius corrupti? Tempore exercitationem adipisci blanditiis quia id quas, odio magnam.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde sequi doloribus, impedit nemo numquam nobis voluptatem, expedita, omnis ea eius corrupti? Tempore exercitationem adipisci blanditiis quia id quas, odio magnam.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde sequi doloribus, impedit nemo numquam nobis voluptatem, expedita, omnis ea eius corrupti? Tempore exercitationem adipisci blanditiis quia id quas, odio magnam.</p>
                    </div>
                </div>
			</div>
			<aside class="sidebar">
				<div class="sidebar__item">
					<form class="form">
						<div class="form-group">
							<input type="text" class="form-control" placeHolder="Search">
						</div>
					</form>
				</div>
				<div class="sidebar__item">
					<div class="accordion">
						<div class="accordion__item -open">
							<div class="accordion__header">
								<h5 class="accordion__title">Results</h5>
							</div>
							<div class="accordion__content">
								<ul>
									<li>
										<a href="#">Category 1</a>
									</li>
									<li>
										<a href="#">Category 1</a>
									</li>
									<li>
										<a href="#">Category 1</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="sidebar__item">
					<h2 class="sidebar__title">Top BLogs</h2>
					<ul class="list-posts">
						<li class="list-posts__item">
							<a href="#" class="cp-card -thumb">
								<div class="cp-card__wrap">
									<div class="cp-card__content">
										<h6 class="cp-card__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h6>
									</div>
									<div class="cp-card__bottom">
										<ul class="cp-card__meta">
											<li>3:45pm</li>
											<li>17 - oct -2021</li>       
										</ul>
									</div>
								</div>
								<figure class="cp-card__figure">
									<img src="assets/images/img-post.png"/>
								</figure>
							</a>
						</li>
						<li class="list-posts__item">
							<a href="#" class="cp-card -thumb">
								<div class="cp-card__wrap">
									<div class="cp-card__content">
										<h6 class="cp-card__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h6>
									</div>
									<div class="cp-card__bottom">
										<ul class="cp-card__meta">
											<li>3:45pm</li>
											<li>17 - oct -2021</li>       
										</ul>
									</div>
								</div>
								<figure class="cp-card__figure">
									<img src="assets/images/img-post.png"/>
								</figure>
							</a>
						</li>
						<li class="list-posts__item">
							<a href="#" class="cp-card -thumb">
								<div class="cp-card__wrap">
									<div class="cp-card__content">
										<h6 class="cp-card__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h6>
									</div>
									<div class="cp-card__bottom">
										<ul class="cp-card__meta">
											<li>3:45pm</li>
											<li>17 - oct -2021</li>       
										</ul>
									</div>
								</div>
								<figure class="cp-card__figure">
									<img src="assets/images/img-post.png"/>
								</figure>
							</a>
						</li>
						<li class="list-posts__item">
							<a href="#" class="cp-card -thumb">
								<div class="cp-card__wrap">
									<div class="cp-card__content">
										<h6 class="cp-card__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </h6>
									</div>
									<div class="cp-card__bottom">
										<ul class="cp-card__meta">
											<li>3:45pm</li>
											<li>17 - oct -2021</li>       
										</ul>
									</div>
								</div>
								<figure class="cp-card__figure">
									<img src="assets/images/img-post.png"/>
								</figure>
							</a>
						</li>
					</ul>
				</div>
			</aside>
        </div>
    </div>
</div>

<?php 

include('partials/footer.php');

?>