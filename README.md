# Yarn, gulp and php files, start biolerplate

## Requisitos

- Python v >= v2.5.0 & < 3.0.0
  - Windows 10 http://www.anthonydebarros.com/2015/08/16/setting-up-python-in-windows-10/
- Node.JS

## Como Instalar

- Crie um repositório importando o Repos, e clone em sua maquina esse novo repositório.

- Run 'yarn' para instalar as dependencias.
- Run 'gulp' para iniciar o desenvolvimento
- Run 'gulp deploy' para criar os assets finais, minificar estilos e scripts.

- Run 'gulp copy-back' para atualizar os arquivos do front na pasta do tema.

## Version

2.0

## Observações

### Windows e dependencias de node_modules

Podem ocorrer problemas ao remover os diretórios criados pelo gulp (Node), onde o Windows afirma que o caminho está como nome muito longo e impede a remoção adequada do diretório.

Para resolver isso, instale o modulo abaixo:

```
#!node

npm install -g rimraf
```

E para remover o diretório, execute:

```
#!node

rimraf C:\_projeto\[DIRETORIO_PARA_REMOVER]
```
