$(function () {
	var $body = $('body');

	$('.main-header .ham').on('click', function (e) {
		e.preventDefault();

		$body.toggleClass('-is-nav');
	});

	$('.main-nav li.-sub > a').on('click', function (e) {
		e.preventDefault();

		$(this).parent().toggleClass('-active');
	});

	$('.cp-tab-filter__tab > a').on('click', function (e) {
		e.preventDefault();

		$('.cp-tab-filter__tab > a').removeClass('-active');
		$(this).addClass('-active');
	});

	$('.cp-filter .cp-filter__btn').on('click', function (e) {
		e.preventDefault();

		$(this).closest('.cp-filter').toggleClass('-all');
	});

	// SLICK

	$('.cp-gallery__view').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		infinite: false
	});

	$('.cp-gallery__thumbs li').on('click', function () {
		$('.cp-gallery__view').slick('slickGoTo', $(this).index());
	});

	$('.slick-custom').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: true,
		infinite: false,

		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					arrows: false
				}
			}
		]
	});

	//PLUGIN MASK

	if (
		$('form').find(
			$('.mask_data, .mask_cep, .mask_cpf, .mask_cnpj, .mask_telefone')
		).length
	) {
		$('.mask_data').mask('99/99/9999');
		$('.mask_cep').mask('99.999-999');
		$('.mask_cpf').mask('999.999.999-99');
		$('.mask_cnpj').mask('99.999.999/9999-99');
		$('.mask_telefone').mask('(99)9999-9999');
	}

	//PLUGIN FANCYBOX

	if ($('.lightbox, .fancybox').length) {
		$('.lightbox, .fancybox').fancybox();
	}

	//PLUGIN SELECT2

	$('.select-styled').select2();
});
