$(function () {
  var $body = $('body'),
    $loader = '<div class="loader">Loading...</div>'

  // var myAjax = {};

  $('.main-header .ham').on('click', function (e) {
    e.preventDefault()

    $body.toggleClass('-is-nav')
  })

  $('.main-nav li.-sub > a').on('click', function (e) {
    e.preventDefault()

    $(this).parent().toggleClass('-active')
  })

  $('.form-select-value .form-control').on('change', function () {
    $('.form-select-value').submit()
  })

  // CONTACT

  $('.cp-newsletter__form').submit(function (e) {
    $('.cp-newsletter__form .alert').remove()

    var formData = {
      action: 'apiSendNewNewsletter',
      email: $('input[name=email]').val(),
    }

    // process the form
    $.ajax({
      type: 'POST',
      url: myAjax.ajaxurl,
      data: formData,
      dataType: 'json',
      encode: true,
    }).done(function (data) {
      if (data.status === 'success') {
        $('.cp-newsletter__form').append('<div class="alert -success">' + data.message + '</div>')
      } else {
        $('.cp-newsletter__form').append('<div class="alert -error">' + data.message + '</div>')
      }
    })

    e.preventDefault()
  })

  $('.form-contact').submit(function (e) {
    // process the form
    var formData = $(this).serialize()

    $(this).find('.alert').remove()

    $.ajax({
      type: 'POST',
      context: this,
      url: myAjax.ajaxurl,
      data: formData,
      dataType: 'json',
      encode: true,
    }).done(function (data) {
      if (data.status === 'success') {
        $(this).append('<div class="alert -success">' + data.message + '</div>')
      } else {
        $(this).append('<div class="alert -error">' + data.message + '</div>')
      }
    })

    e.preventDefault()
  })

  $('.cp-filter__form').submit(function (e) {
    e.preventDefault()

    var post_url = $(this)[0].action.split('?')[0],
      form_data = $(this).serialize()

    var posting = $.post(post_url, form_data)

    $('.archive').addClass('-loaded').append($loader)

    posting.done(function (data) {
      var productItems = $(data).find('.archive').html()

      $('.archive').removeClass('-loaded').find('.loader').remove()

      $('.archive').html(productItems)
    })
  })

  $('.accordion').on('click', '.accordion__header', function () {
    $(this).parent().toggleClass('-open')
  })

  $('.cp-timeline__nav').on('click', 'span', function () {
    var index = $(this).index()

    $('.cp-timeline__nav span').removeClass('-active')
    $('.cp-timeline__item').removeClass('-active')
    $(this).addClass('-active')
    $('.cp-timeline__item').eq(index).addClass('-active')
  })

  // SLICK

  $('.cp-slider').slick({
    dots: true,
    arrows: false,
  })

  $('.cp-carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    infinite: false,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  })

  $('.cp-testimonials').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    arrows: false,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '0px',
        },
      },
    ],
  })

  $('.cp-gallery__view').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    infinite: false,
  })

  $('.cp-gallery__thumbs li').on('click', function () {
    $('.cp-gallery__view').slick('slickGoTo', $(this).index())
  })

  $('.slick-custom').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: true,
    infinite: false,

    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false,
        },
      },
    ],
  })

  //PLUGIN MASK

  if ($('form').find($('.mask_data, .mask_cep, .mask_cpf, .mask_cnpj, .mask_telefone')).length) {
    $('.mask_data').mask('99/99/9999')
    $('.mask_cep').mask('99.999-999')
    $('.mask_cpf').mask('999.999.999-99')
    $('.mask_cnpj').mask('99.999.999/9999-99')
    $('.mask_telefone').mask('(99)9999-9999')
  }

  //PLUGIN FANCYBOX

  if ($('.lightbox, .fancybox').length) {
    $('.lightbox, .fancybox').fancybox()
  }

  //PLUGIN SELECT2

  $('.select-styled').select2()
})
