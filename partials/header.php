
<?php 

include('head.php'); 

?>


<header class="main-header">
    <div class="container">
        <a href="./" class="main-logo">
            <img src="assets/images/logo.png"/>
        </a>

        <div class="header-nav">
            <ul class="main-nav">
                <li><a href="#">Home</a></li>
                <li>
                    <a href="">Honest FX Services</a>
                </li>
                <li>
                    <a href="">About us</a>
                </li>
                <li>
                    <a href="">Blog</a>
                </li>
                <li>
                    <a href="">Contact us</a>
                </li>
                <li><a href="">Members</a></li>
            </ul>
        </div>

        <a href="#" class="ham">
            <span class="item"></span>
            <span class="item"></span>
            <span class="item"></span>
        </a>
    </div>
</header>

