<footer class="main-footer">
    <div class="container">
        <div class="main-footer__infos">
            <a href="./" class="main-logo">
                <img src="assets/images/logo.png"/>
            </a>
        </div>

        <div class="main-footer__nav">
            <div class="main-footer__item">
                <ul class="main-footer__list">
                    <li><a href="#">Forex Analysis & Information group</a></li>
                    <li><a href="#">Online Trading Course</a></li>
                    <li><a href="#">Advanced Trader</a></li>
                    <li><a href="#">Mentoring</a></li>
                </ul>
            </div>

            <div class="main-footer__item">
                <h4 class="main-footer__title">Company</h4>
                <ul class="main-footer__list">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact US</a></li>
                </ul>
            </div>
        </div>

        <div class="main-footer__nav">
            <div class="main-footer__item">
                <h4 class="main-footer__title">Legal & Compliance</h4>
                <ul class="main-footer__list">
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="assets/vendors/jquery.js"></script>
<script type="text/javascript" src="assets/vendors/slick.min.js"></script>
<script type="text/javascript" src="assets/vendors/jquery-modal.min.js"></script>
<script type="text/javascript" src="assets/vendors/select2.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>