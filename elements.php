<?php 

include('partials/head.php');

?>

<div class="page-elements">

    <h1 class="text-center">Elements</h1>

    <div class="panel elements-item">

        <h2 class="panel__title">Cores</h2>

        <div class="bg-primary panel">Primary color</div>
        <div class="bg-secondary panel">Secondary color</div>
        <div class="bg-text panel">Text color</div>
        <div class="bg-link panel">Link color</div>
    </div>

    <div class="panel">

        <h2 class="panel__title">Botoes</h2>

        <a href="#" class="btn">Button Link</a>
        <button class="btn">Button</button>
        <button class="btn -primary">Button primary</button>
        <button class="btn -secondary">Button secondary</button>
        <button class="btn -border">Button border</button>
    </div>

    <div class="panel">

        <h2 class="panel__title">Formulário</h2>

        <form class="form">
            <div class="form-row">
                <div class="form-group">
                    <label>Text</label>
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group">
                    <label>Text</label>
                    <input type="date" name="name" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group">
                    <label>Text</label>
                    <input type="number" name="name" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group">
                    <label>Text</label>
                    <input type="text" name="name" class="form-control error">
                    <span class="error">Mensagem de erro</span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group">
                    <label>Text</label>
                    <textarea class="form-control"></textarea>
                </div>
            </div>
            <div class="form-message">
                Mensagem de sucesso
            </div>
            <div class="form-row">
                <div class="form-group">
                    <input type="submit" class="btn -primary" value="Quero saber mais"/>
                </div>
            </div>
        </form>

    </div>

    <div class="panel">

        <h2 class="panel__title">Tipografia</h2>

        <h1>H1</h1>
        <h2>H2</h2>
        <h3>H3</h3>
        <h4>H4</h4>
        <h5>H5</h5>
        <h6>H6</h6>

        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quo autem amet deleniti sit eum qui voluptatem optio voluptatibus magni laboriosam rem magnam totam ab omnis! Suscipit libero ex eaque?</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quo autem amet deleniti sit eum qui voluptatem optio voluptatibus magni laboriosam rem magnam totam ab omnis! Suscipit libero ex eaque?</p>

    </div>

    <div class="panel">

        <h2 class="panel__title">Bloco de texto</h2>

        <div class="block-txt">
            <h2>Title</h2>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure eligendi modi beatae dolorem incidunt ex nam ipsam dolor libero eum necessitatibus id sed aut odit voluptate, vitae consequatur tempore cumque?</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure eligendi modi beatae dolorem incidunt ex nam ipsam dolor libero eum necessitatibus id sed aut odit voluptate, vitae consequatur tempore cumque?</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure eligendi modi beatae dolorem incidunt ex nam ipsam dolor libero eum necessitatibus id sed aut odit voluptate, vitae consequatur tempore cumque?</p>
            <figure>
                <img src="assets/images/img.jpg"/>
            </figure>
        </div>

    </div>

    <div class="panel">

        <h2 class="panel__title">Tabela</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>item</th>
                    <th>item</th>
                    <th>item</th>
                    <th>item</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>item</td>
                    <td>item</td>
                    <td>item</td>
                    <td>item</td>
                </tr>
                <tr>
                    <td>item</td>
                    <td>item</td>
                    <td>item</td>
                    <td>item</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="panel">

        <h2 class="panel__title">Icones</h2>

        <nav class="list-icons">

            <i class="svg-icon">
                <svg width="40" height="40">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprite.svg#play"></use>
                </svg>
            </i>

            <i class="svg-icon">
                <svg width="40" height="40">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprite.svg#arrow"></use>
                </svg>
            </i>

            <i class="svg-icon">
                <svg width="40" height="40">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprite.svg#facebook"></use>
                </svg>
            </i>

            <i class="svg-icon">
                <svg width="40" height="40">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprite.svg#youtube"></use>
                </svg>
            </i>

            <i class="svg-icon">
                <svg width="40" height="40">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/images/sprite.svg#twitter"></use>
                </svg>
            </i>

        </nav>

    </div>

</div>

<?php 

include('partials/footer.php');

?>