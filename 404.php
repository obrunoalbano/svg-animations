<?php 

include('partials/header.php');

?>

<div class="page-none">
    <h2 class="page-none__title">404</h2>
    <p class="page-none__text">O link clicado pode estar quebrado ou a página <br/> pode ter sido removida.</p>
    <p>Visite a <a href="./">página inicial</a> ou <a href="./contato/">entre em contato</a></p>
</div>

<?php 

include('partials/footer.php');

?>